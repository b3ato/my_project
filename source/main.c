#include <stdio.h>
#include "dice.h"

int main() {
	// Take user input for number of faces
	int faces;
	do {
		printf("Choose number of faces: ");
		scanf("%d", &faces);
	} while (faces <= 0);
	// Initialize random seed
	initializeSeed();
	// Generate random number from 1 to 6
	printf("Let's roll the dice: %d\n", rollDice(faces));
	return 0;
}
